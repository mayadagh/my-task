
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Joi = require('joi');
const viewModel= require('./view');
const permissionModel = require('./permission')

 
const roleSchema =new mongoose.Schema({
  roleName:{type : String , require:true},
  action: [{ type: Schema.Types.ObjectId, ref: 'Permission' }],

  view: [{ type: Schema.Types.ObjectId, ref: 'View' }],



});
 
let collectionName = 'qpix12.02.03-Role';

const Role = mongoose.model('Role',roleSchema,collectionName);

module.exports = Role;
 
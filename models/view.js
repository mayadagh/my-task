                             
const mongoose=require('mongoose');
const Schema = mongoose.Schema;
const permissionModel=require('./permission');
const Joi = require('joi');
 
const viewSchema=new mongoose.Schema({
 
    
    viewName:{type : String , require:true},
 
 
});

// function validateView(view){
//     const schema={
//         viewName:Joi.require().String() ,
//     }
//     return Joi.validate(view,schema);
// }
let collectionName='qpix12.02.03-View';

const View=mongoose.model('View',viewSchema,collectionName);

module.exports=View;
 
 const  mongoose= require('mongoose');
const Joi = require('joi');
const Schema = mongoose.Schema;

const viewModel=require('./view');

 

const permissionSchema=new mongoose.Schema({
    permissionName: { type: String,require:true },
    IsGlobal: Boolean ,
    view: [{ type: Schema.Types.ObjectId, ref: 'View' }],

});
// function validatePermission(permission){
//     const schema={
//         permissionName:Joi.string().require(),
//         IsGlobal:Joi.Boolean() 
//     }
//     return Joi.validate(permission,schema);
// }

let collectionName = 'qpix12.02.03-Permission';

const Permission = mongoose.model('Permission', permissionSchema,collectionName);
 module.exports = Permission;
// module.exports=permissionSchema;
// module.exports=validatePermission;


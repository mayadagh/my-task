const express=require('express');
const router = express.Router();
const viewModel=require('../models/view');
const PermissionModel=require('../models/permission');

 
 
// 1- List
router.get('/',async (req,res,next)=>{
  var view = await viewModel.find();
    res.render('view',{view:view});
 });
// 3- show get /view/:id
router.get('/:id',async (req,res,next)=>{
  try{
    var view = await viewModel.findOne({_id: req.params.id});
    if(view){
      res.render('show-view',{view:view});

      
    }else{
      res.status(404).send({error: "view view not found"})
    }
    
  }catch(err){
    res.status(400).send({error: err.message});
  }
});
router.get('/view/add',async (req,res)=>{
  
    res.render('add-view');
 

}); 
// 2- create view /view
router.post('/',async (req,res,next)=>{
  var new_view = new viewModel(req.body);
  try{
    var view = await new_view.save();
    res.redirect('/view');
  }catch(err){
    res.status(400).send({error: err.message});
  }
});

// 4- update put /view/:id
router.put('/:id',async (req,res,next)=>{
  try{
    var view = await viewModel.findByIdAndUpdate(req.params.id, req.body);
    if(view){ 

      res.render('view',{view:view});
    }else{
      res.status(404).send({error: "view not found"})
    }
  }catch(err){
    res.status(400).send({error: err.message});
  }
});
// 5- delete delete /view/:id
router.delete('/:id',async (req,res,next)=>{
  try{
    var result = await viewModel.delete({_id: req.params.id});
    if(result){
      res.redirect('view');
    }else{
      res.status(404).send({error: "view not found"})
    }
  }catch(err){
    res.status(400).send({error: err.message});
  }
});
module.exports = router;


const express=require('express');
const router = express.Router();
const Permission=require('../models/permission');

 
 
// 1- List
router.get('/',async (req,res,next)=>{
  
    var permission = await Permission.find();
    res.render('permission',{permission:permission});
  });
  // 3- show get /permission/:id
router.get('/:id',async (req,res,next)=>{
  var yy=req.body._id;

    try{
      var permission = await Permission.findById(req.params.id);
      if(permission){
    res.render('show-permission',{permission:permission});
     
      }else{
        res.status(404).send({error: " permission not found"})
      }
      
    }catch(err){
      res.status(400).send({error: err.message});
    }
  });
  
router.get('/perm/add',async (req,res)=>{
  var perm = await Permission.find({}).select(["-__v"]);

  var view = await viewModel.find({}).select(["-__v"]);
 

    
  console.log(view);
   
      res.render('add-view',{view:view,perm:perm});
 
}); 
 // 2- create permission /permission
router.post('/',async (req,res)=>{
  var permission = new Permission(req.body);
  try{
    var result = await permission.save();
    res.redirect('/action');
  }catch(err){
    res.status(400).send({error: err.message});
  }
});
// 4- update put /permission/:id
router.put('/:id',async (req,res,next)=>{
  console.log(req.body);
    try{
      var permission = await Permission.findByIdAndUpdate(req.params.id, req.body);
      if(permission){ 
        res.render('permission',{permission:permission});
      }else{
        res.status(404).send({error: "permission not found"})
      }
    }catch(err){
      res.status(400).send({error: err.message});
    }
  });
// 5- delete delete /permission/:id
router.get('del/:id',async (req,res,next)=>{
    try{
      var result = await Permission.delete({_id: req.params.id});
      if(result){
        res.redirect('permission',{permission:permission});
      }else{
        res.status(404).send({error: "permission not found"})
      }
    }catch(err){
      res.status(400).send({error: err.message});
    }
  });

  

  

  module.exports = router;

